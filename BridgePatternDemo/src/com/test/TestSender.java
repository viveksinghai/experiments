package com.test;

import com.test.Message.EmailMessage;
import com.test.Message.Message;
import com.test.Message.TextMessage;
import com.test.MessageSender.EmailMessageSender;
import com.test.MessageSender.MessageSender;
import com.test.MessageSender.TextMessageSender;

public class TestSender {

	public static void main(String[] args) {
		
		
		MessageSender messageSender = new EmailMessageSender();
		Message message = new  EmailMessage(messageSender);
		message.send();
		
		
		MessageSender textMessageSender = new TextMessageSender();
		Message message2 = new  TextMessage(textMessageSender);
		message2.send();

	}

}
