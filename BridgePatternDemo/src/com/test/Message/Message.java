package com.test.Message;

import com.test.MessageSender.MessageSender;

public abstract class Message {
	public MessageSender sender;
	
	public Message(MessageSender sender) {
		this.sender = sender;
	}
	
	public abstract void send();

}
