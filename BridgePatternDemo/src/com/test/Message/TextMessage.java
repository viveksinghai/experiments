package com.test.Message;

import com.test.MessageSender.MessageSender;

public class TextMessage extends Message{

	public TextMessage(MessageSender sender) {
		super(sender);
	}

	@Override
	public void send() {
		sender.sendMessage();
	}

}
